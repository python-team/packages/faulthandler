faulthandler (3.1-2) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Strip unusual field spacing from debian/control.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Ondřej Nový <onovy@debian.org>  Sat, 20 Jul 2019 00:29:47 +0200

faulthandler (3.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * Convert git repository from git-dpm to gbp layout

  [ Stewart Ferguson ]
  * Imported Upstream version 3.1
  * Adding myself to Uploaders
  * Adding lintian override explaining no p3- package
  * Installing *.pth to /usr/share and linking to dist-packages to solve
    lintian error
  * Bumping compat 9 -> 12
  * Updating Standards-Version 3.9.8 -> 4.3.0 (no changes required)
  * Adding hardening rules
  * Adding Rules-Requires-Root to control
  * Adding lintian overrides for *.pth

 -- Stewart Ferguson <stew@ferg.aero>  Sat, 09 Mar 2019 11:56:31 +0100

faulthandler (2.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release (Closes: #827356)
  * Fixed VCS URL (https)
  * d/copyright
    - Changed to new machine-readable format
    - Changed Upstream-Name email
  * Bumped debhelper version to 9
  * wrap-and-sort -t -a
  * d/rules: Migrated to pybuild
  * d/s/options: Use default compression
  * d/watch: Use pypi.debian.net
  * d/copyright: Added myself to Debian part
  * Standards-Version is 3.9.8 now (no more changes needed)
  * Enabled autopkgtest-pkg-python testsuite
  * Changed homepage to new one

 -- Ondřej Nový <onovy@debian.org>  Fri, 19 Aug 2016 21:35:06 +0200

faulthandler (2.0-2) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Javi Merino ]
  * Team upload
  * Convert to dh_python2
  * Remove suggests on non-existent package python-faulthandler-dbg

 -- Javi Merino <vicho@debian.org>  Thu, 28 Aug 2014 18:51:07 -0700

faulthandler (2.0-1) unstable; urgency=low

  [ Miriam Ruiz ]
  * New upstream release
  * Fixed order of debhelper commands in debian/rules: moved dh_pysupport
    before dh_strip -a
  * Upgraded Standards-Version from 3.9.1 to 3.9.2
  * Added targets build-arch and build-indep to debian/rules
  * Changed homepage in debian/control to
    https://github.com/haypo/faulthandler/wiki/

  [ Jakub Wilk ]
  * Add Vcs-* fields.

 -- Miriam Ruiz <little_miry@yahoo.es>  Fri, 14 Oct 2011 00:52:14 +0200

faulthandler (1.3-1) unstable; urgency=low

  * Initial release. Closes: #612363

 -- Miriam Ruiz <little_miry@yahoo.es>  Tue, 08 Feb 2011 01:06:39 +0100
